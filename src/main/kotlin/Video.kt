package me.edwin.kotlin

import kotlinx.browser.window
import kotlinx.coroutines.*

data class Video(val id: Int, val title: String, val speaker: String, val videoUrl: String)

val initUnwatchedVideos = listOf(
        Video(1, "Building and breaking things", "John Doe", "https://youtu.be/PsaFVLr8t4E"),
        Video(2, "The development process", "Jane Smith", "https://youtu.be/PsaFVLr8t4E"),
        Video(3, "The Web 7.0", "Matt Miller", "https://youtu.be/PsaFVLr8t4E")
)

val initWatchedVideos = listOf(
        Video(4, "Mouseless development", "Tom Jerry", "https://youtu.be/DiwkV8K7cFU")
)


suspend fun fetchVideo(id: Int): Video =
        window.fetch("https://my-json-server.typicode.com/kotlin-hands-on/kotlinconf-json/videos/$id")
                .await()
                .json()
                .await()
                .unsafeCast<Video>()

suspend fun fetchVideos(): List<Video> = coroutineScope {
    (1..25).map { id ->
        async {
            fetchVideo(id)
        }
    }.awaitAll()
}