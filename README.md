[![official JetBrains project](https://jb.gg/badges/official.svg)](https://confluence.jetbrains.com/display/ALL/JetBrains+on+GitHub)
[![GitHub license](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

# Building Web Applications with React and Kotlin JS Hands-On Lab

This repository is the code corresponding to the hands-on lab Building Web Applications with React and Kotlin JS.

### Please see the [Kotlin Lab](https://play.kotlinlang.org/hands-on/Building Web Applications with React and Kotlin JS/)

#### Run Demo App

1. [install gradle](https://gradle.org/install/)
1. ```gradle run```
1. Browser should open at [http://localhost:8080](http://localhost:8080)